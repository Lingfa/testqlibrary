#include <math.h>
#include "ComputeLib.h"

double ComputeLib::sum(double a, double b)
{
    return a + b;
}

double ComputeLib::product(double a, double b)
{
    return a * b;
}

double ComputeLib::squreRootOf(double a)
{
    return sqrt(a);
}

//===========================

extern "C" COMPUTELIB_EXPORT double _sum(double x, double y)
{
    return ComputeLib::sum(x, y);
}

extern "C" COMPUTELIB_EXPORT double _product(double x, double y)
{
    return ComputeLib::product(x, y);
}

extern "C" COMPUTELIB_EXPORT double _squreRootOf(double x)
{
    return ComputeLib::squreRootOf(x);
}
