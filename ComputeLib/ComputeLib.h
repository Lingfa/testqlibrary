#ifndef COMPUTELIB_H
#define COMPUTELIB_H

#define COMPUTELIB_EXPORT __declspec(dllexport)

class COMPUTELIB_EXPORT ComputeLib
{
public:
    static double sum(double a, double b);
    static double product(double a, double b);
    static double squreRootOf(double a);
};

#endif // COMPUTELIB_H
