CONFIG -= qt

TEMPLATE = lib

CONFIG += c++11

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    ComputeLib.cpp

HEADERS += \
    ComputeLib.h

# Default rules for deployment.
unix {
    target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target
