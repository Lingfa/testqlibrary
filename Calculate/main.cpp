#include <QDebug>
#include <QLibrary>
#include <QCoreApplication>

typedef double (*BinaryFunction)(double, double);
typedef double (*UnaryFunction)(double);

int main(int argc, char *argv[])
{
    //QCoreApplication a(argc, argv);

#ifdef _DEBUG
    const QString path = "../../ComputeLib/debug";
#else
    const QString path = "../../ComputeLib/release";
#endif
    const QString fileName = path + "/ComputeLib.dll";
    QLibrary lib(fileName);
    if (lib.load() && lib.isLoaded()) {
        qDebug() << fileName << "has been loaded correctly.";

        const char exportSum[] = "_sum";
        BinaryFunction sum = (BinaryFunction)lib.resolve(exportSum);
        if (sum) {
            qDebug() << "1+2 =" << sum(1, 2);
            qDebug() << "3+4 =" << sum(3, 4);
        } else {
            qDebug() << "Cannot solve" << exportSum;
        }

        const char exportProduct[] = "_product";
        BinaryFunction multiply = (BinaryFunction)lib.resolve(exportProduct);
        if (multiply) {
            qDebug() << "1*2 =" << multiply(1, 2);
            qDebug() << "3*4 =" << multiply(3, 4);
        } else {
            qDebug() << "Cannot solve" << exportProduct;
        }

        const char exportSqrt[] = "_squreRootOf";
        UnaryFunction mySqrt = (UnaryFunction)lib.resolve(exportSqrt);
        if (mySqrt) {
            qDebug() << "mySqrt(2) =" << mySqrt(2);
            qDebug() << "mySqrt(3) =" << mySqrt(3);
        } else {
            qDebug() << "Cannot solve" << exportSqrt;
        }

    } else {
        qDebug() << "Failed to load" << fileName;
    }

    return 0;
    //return a.exec();
}
